import variable from "../variables/platform";

export default (variables = variable) => {
  const spinnerTheme = {
    height: 80,
    ".primary" : {
      color: variable.btnPrimaryColor
    }
  };

  return spinnerTheme;
};
