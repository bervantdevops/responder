import React from 'react';
import {Platform, StatusBar, StyleSheet, View, SafeAreaView} from 'react-native';
import { Root } from 'native-base'
import {AppLoading, Asset, Font, Constants} from 'expo';
import { Ionicons } from '@expo/vector-icons';
import {Provider} from 'react-redux';
import AppNavigator from './src/navigation/AppNavigator';
import {store} from './src/store/rematch'
import {getPersistor} from '@rematch/persist'
import {PersistGate} from 'redux-persist/lib/integration/react'
import {StyleProvider} from "native-base";
import getTheme from './native-base-theme/components';
import variables from './native-base-theme/variables';
import {Splash} from 'my-components'
const persistor = getPersistor();

export default class App extends React.Component {
  state = {
    isLoadingComplete: false,
  };

  render() {
    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <SafeAreaView style={styles.container}>
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
        </SafeAreaView>
      );
    } else {
      return this.renderRematchApp();
    }
  }


  renderRematchApp() {
    return (
      <StyleProvider style={getTheme(variables)}>
        <Root>
          <View style={styles.container}>
            {Platform.OS === 'ios' &&  <StatusBar backgroundColor="blue" barStyle="light-content" /> }
            <Provider store={store}>
              <PersistGate persistor={persistor} loading={<Splash/>}>
                <AppNavigator/>
              </PersistGate>
            </Provider>
          </View>
        </Root>
      </StyleProvider>
    );
  }


  _loadResourcesAsync = async () => {
    return Promise.all([
      Asset.loadAsync([
        require('./src/assets/images/robot-dev.png'),
        require('./src/assets/images/robot-prod.png'),
      ]),
      Font.loadAsync({
        // This is the font that we are using for our tab bar
        ...Ionicons.font,
        // We include SpaceMono because we use it in HomeScreen.js. Feel free
        // to remove this if you are not using it in your app
        'space-mono': require('./src/assets/fonts/SpaceMono-Regular.ttf'),
        'Roboto': require('native-base/Fonts/Roboto.ttf'),
        'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
      }),
    ]);
  };

  _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error);
  };

  _handleFinishLoading = () => {
    this.setState({isLoadingComplete: true});
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f5f5',
    paddingTop: Platform.OS === "ios" ? Constants.statusBarHeight : 0,
  },
});
