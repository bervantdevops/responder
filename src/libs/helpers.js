import React from 'react'
import { AsyncStorage, Dimensions } from 'react-native';
import  * as app from '../../app.json'
import {Badge, Text, Toast} from 'native-base'
import Pusher from 'pusher-js/react-native';

const { height, width } = Dimensions.get('window');
const ASPECT_RATIO = width / height;

const LATITUDE_DELTA = 0.009922;
const LONGITUDE_DELTA = ASPECT_RATIO * LATITUDE_DELTA;

export function query_string(obj, prefix) {
  var str = [],
    p;
  for (p in obj) {
    if (obj.hasOwnProperty(p)) {
      var k = prefix
        ? prefix + '[' + p + ']'
        : p,
        v = obj[p];
      str.push((v !== null && typeof v === 'object')
        ? query_string(v, k)
        : encodeURIComponent(k) + '=' + encodeURIComponent(v))
    }
  }
  return str.join('&')
}

export function url(path, params) {
  const { config } = app;
  let base_url = config.prod
    ? config.base_url
    : config.dev_url;
  let url = base_url + path;
  let full_url = url;
  if (params) {
    params = query_string(params);
    full_url = url.includes('?')
      ? url + "&" + params
      : url + '?' + params;
  }

  console.log("URL =>", full_url);
  return full_url;
}

export const saveState = (state) => {
  try {
    const serializedState = JSON.stringify(state);
    AsyncStorage.setItem('state', serializedState)
  } catch (err) {
    console.log("saveState: ", err)
  }
};

export async function getItemFromState(key) {
  try {
    const value = await AsyncStorage.getItem(key);

    return JSON.parse(value)
  } catch (error) {

    return null;
  }
}

export function createReducer(initialState, handlers) {
  return function reducer(state = initialState, action) {
    if (handlers.hasOwnProperty(action.type)) {
      return handlers[action.type](state, action)
    } else {
      return state
    }
  }
}

export function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email.toLowerCase());
}

export function validatePhone(str) {
  var patt = /^\+?1?\s*?\(?\d{3}(?:\)|[-|\s])?\s*?\d{3}[-|\s]?\d{4}$/;
  return patt.test(str);
}

export async function authHeader() {
  // return authorization header with jwt token
  const user = await getItemFromState('user');

  if (user && user.token) {
    return {
      'Authorization': 'Bearer ' + user.token
    };
  } else {
    return {};
  }

}

export async function getUser() {
  // return authorization header with jwt token
  return await getItemFromState('user');
}

export async function getProfile() {
  // return authorization header with jwt token
  return await getItemFromState('profile');
}


export function auth(user) {
  return {key: user.pk, token: user.token}
}

export function getError(errors, key) {
  try {
    return errors[key][0]
  } catch (e) {
    return null;
  }
}

export function showToast(alert) {
  if (alert) {
    Toast.show({text: alert.message, position: 'bottom', duration: 3000, type: alert.type})
  }
}

export function getLatLonDiffInMeters(lat1, lon1, lat2, lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2 - lat1); // deg2rad below
  var dLon = deg2rad(lon2 - lon1);
  var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c; // Distance in km
  return d * 1000;
}

function deg2rad(deg) {
  return deg * (Math.PI / 180)
}

export function status(resource, status) {
  try {
    const arr = typeof(status) === 'array'
      ? status
      : [status];
    let check = null;

    switch (typeof(resource)) {
      case 'object':
        check = resource.status;
        break;
      case 'array':
        check = resource['status'];
        break;
      case 'string':
        check = resource;
        break;
      default:
        check = null;
        break;
    }

    return arr.includes(check)

  } catch (error) {
    return false;
  }
}

export async function saveItem(item, selectedValue) {
  try {
    const value = typeof(selectedValue) === 'string' ? selectedValue : JSON.stringify(selectedValue)
    await AsyncStorage.setItem(item, value);
  } catch (error) {
    console.error('AsyncStorage error: ' + error.message);
  }
}

export function primaryPhoneNumber(item) {
  try {
    let primary = item
      .phoneNumbers
      .filter((item) => {
        return item.primary
      })

    if (primary.length > 0) return primary[0].number
    return item.phoneNumbers[0].number
  } catch (ex) {
    return ''
  }
}

export const loan_status_label = (item) => {
  const istyles = {
    squareBadge: {
      borderRadius: 5,
      marginLeft: 10
    },
    badgeText: {
      fontSize: 11
    },
  };

  switch (item.status) {
    case 'unpaid':
      return (
        <Badge danger style={istyles.squareBadge}>
          <Text style={istyles.badgeText}>UNPAID</Text>
        </Badge>
      );
    case 'partially-paid':
      return (
        <Badge warnig style={istyles.squareBadge}>
          <Text style={istyles.badgeText}>PARTIAL</Text>
        </Badge>
      );
    case 'paid':
      return (
        <Badge success style={istyles.squareBadge}>
          <Text>PAID</Text>
        </Badge>
      );
    default:
      return (
        <Badge danger style={istyles.squareBadge}>
          <Text style={istyles.badgeText}>UNPAID</Text>
        </Badge>
      );
  }
};

export function renderError(errors, field) {
  let error;
  if (errors && getError(errors, field)) {
    return (<Text danger small style={[{marginBottom: 10}]}>{getError(errors, field)}</Text>)
  }
}

export const hasError = (errors, field) => {
  return (errors && !!getError(errors, field));
};

export function uniqid (prefix, more_entropy) {
  if (typeof prefix === 'undefined') {
    prefix = "";
  }

  var retId;
  var formatSeed = function (seed, reqWidth) {
    seed = parseInt(seed, 10).toString(16); // to hex str
    if (reqWidth < seed.length) { // so long we split
      return seed.slice(seed.length - reqWidth);
    }
    if (reqWidth > seed.length) { // so short we pad
      return Array(1 + (reqWidth - seed.length)).join('0') + seed;
    }
    return seed;
  };

  // BEGIN REDUNDANT
  if (!this.php_js) {
    this.php_js = {};
  }
  // END REDUNDANT
  if (!this.php_js.uniqidSeed) { // init seed with big random int
    this.php_js.uniqidSeed = Math.floor(Math.random() * 0x75bcd15);
  }
  this.php_js.uniqidSeed++;

  retId = prefix; // start with prefix, add current milliseconds hex string
  retId += formatSeed(parseInt(new Date().getTime() / 1000, 10), 8);
  retId += formatSeed(this.php_js.uniqidSeed, 5); // add seed hex string
  if (more_entropy) {
    // for more entropy we add a float lower to 10
    retId += (Math.random() * 10).toFixed(8).toString();
  }

  return retId;
}

export const makeCircle = size => ({
  height: size,
  width: size,
  borderRadius: size / 2,
});

export const makeHitSlop = size => ({
  right: size,
  left: size,
  bottom: size,
  top: size,
});


export const pusher = () => {
  return new Pusher(app.pusher.key, {
    cluster: app.pusher.cluster,
    forceTLS: true,
    activityTimeout: 300
  });
};

export function regionFrom(location) {
  const oneDegreeOfLongitudeInMeters = 111.32 * 1000;
  const circumference = (40075 / 360) * 1000;
  const { longitude, latitude, accuracy } = location;
  const latDelta = accuracy * (1 / (Math.cos(latitude) * circumference));
  const lonDelta = (accuracy / oneDegreeOfLongitudeInMeters);

  return {
    latitude: latitude,
    longitude: longitude,
    latitudeDelta: LATITUDE_DELTA,
    longitudeDelta: LONGITUDE_DELTA,
  };
}
