import { Helpers } from 'my-components'
import axios from 'axios'
import { store } from '../../../store/rematch'

export default {
  registerPushToken: async (token) => {
    const auth  =  await auth_header();
    return axios.post(Helpers.url('profile/push-token'), {push_token: token}, {headers: auth})
      .then( resp => {
        return resp.data;
      })
  },
  logout: async (paylod) => {
    const auth  =  await auth_header();
    return axios.get(Helpers.url('member/logout'), {headers: auth})
      .then( resp => {
        return resp.data;
      })
  },
  myProfile: async (payload) => {
    const auth  =  await auth_header();
    return axios.get(Helpers.url('member/profile'), {headers: auth})
      .then( resp => {
        return resp.data;
      })
  },
  getCounties: async () => {
    return axios.get(Helpers.url('counties'),)
      .then( resp => {
        return resp.data;
      })
  },
  updateProfile: async (payload) => {
    const auth  =  await auth_header();
    return axios.post(Helpers.url('member/profile/update'),payload, {headers: auth})
      .then( resp => {
        return resp.data;
      })
  },

  toggleOnlineStatus: async (payload) => {
    const auth  =  await auth_header();
    return axios.post(Helpers.url('member/online'),payload, {headers: auth})
      .then( resp => {
        return resp.data;
      })
  },

  getUserProfile: async (user_id) => {
    const auth  =  await auth_header();
    return axios.get(Helpers.url(`member/users/${user_id}/profile`), {headers: auth}).then(resp => {
      return resp.data;
    })
  },

  myRequests: async () => {
    const auth  =  await auth_header();
    return axios.get(Helpers.url(`member/emergencies`), {headers: auth}).then(resp => {
      return resp.data;
    })
  },

  acceptRequest: async (id) => {
    const auth  =  await auth_header();
    return axios.post(Helpers.url(`member/emergencies/${id}/accept`), {}, {headers: auth})
      .then( resp => {
        return resp.data;
      })
  },
  startEvacuation: async (id) => {
    const auth  =  await auth_header();
    return axios.post(Helpers.url(`member/emergencies/${id}/start`), {}, {headers: auth})
      .then( resp => {
        return resp.data;
      })
  },
  cancelRequest: async (id) => {
    const auth  =  await auth_header();
    return axios.post(Helpers.url(`member/emergencies/${id}/cancel`), {}, {headers: auth})
      .then( resp => {
        return resp.data;
      })
  },
  completeRequest: async (id) => {
    const auth  =  await auth_header();
    return axios.post(Helpers.url(`member/emergencies/${id}/complete`), {}, {headers: auth})
      .then( resp => {
        return resp.data;
      })
  },
}

async function auth_header() {
  const  { user } = await store.getState();

  if (user && user.accessToken) {
    return {
      'Authorization': 'Bearer ' + user.accessToken
    };
  } else {
    return {};
  }
}
