import {connect} from "react-redux";
import Home from '../components/home';
const mapStateToProps = state => ({
  user: state.user,
  // requests: state.requests.history,
  currentRequest: state.requests.current
});

const mapDispatchToProps = dispatch => ({
  changeOnlineStatus: (payload) => dispatch.user.onlineStatus(payload),
  cancelRequest: (request_id) => dispatch.requests.cancelRequest(request_id),
  acceptRequest: (request_id) => dispatch.requests.acceptRequest(request_id),
  startRequest: (request_id) => dispatch.requests.startRequest(request_id),
  completeRequest: (request_id) => dispatch.requests.completeRequest(request_id),
  updateRequest: (request) => dispatch.requests.updateRequest(request),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
