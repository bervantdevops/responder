import {connect} from "react-redux";
import Contacts from '../components/contacts';

const mapStateToProps = state => ({
  user: state.user,
  contacts: state.contacts
});

const mapDispatchToProps = dispatch => ({
  fetchContacts: (params) => dispatch.contacts.all(params),
  addContact: (params) => dispatch.contacts.addContact(params),
  editContact: (id, params) => dispatch.contacts.editContact(id, params),
  removeContact: (id, index) => dispatch.contacts.removeContact(id, index)
});

export default connect(mapStateToProps, mapDispatchToProps)(Contacts);