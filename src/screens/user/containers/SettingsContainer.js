import {connect} from "react-redux";
import Settings from '../components/profile/settings';

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  timeline: (params) => dispatch.posts.timeline(params),
  logout: () => dispatch.user.logout()
});

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
