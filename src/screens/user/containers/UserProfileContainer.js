import {connect} from "react-redux";
import UserProfile from '../components/profile/show';

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);