import service from '../module/userService'

export default {
  state: {
    history: [],
    current: null
  }, // initial state
  reducers: {
    // handle state changes with pure functions
    set(state, payload) {
      return {...state, history: payload};
    },
    add(state, payload){
      return {...state, current: payload}
    },
    accept(state, payload){
      return {...state, current: payload }
    },
    cancel(state, payload){
      return {...state, current: null }
    },
    complete(state){
      return {...state, current: null }
    },
    update(state, currentRequest){
      return {...state, current: currentRequest }
    },
    clear(state){
      return {history: [], currentRequest: null }
    },
  },
  effects: (dispatch) => ({
    async all(params, rootState) {
      return service.myContacts(params).then( data => {
        dispatch.requests.set(data.data);
      });
    },
    async acceptRequest(id, rootState) {
      return service.acceptRequest(id).then( data => {
        dispatch.requests.accept(data.data);
      });
    },
    async startRequest(id, rootState) {
      return service.startEvacuation(id).then( data => {
        dispatch.requests.update(data.data);
      });
    },
    async cancelRequest(id, rootState) {
      return service.cancelRequest(id).then( data => {
        dispatch.requests.cancel();
      });
    },
    async completeRequest(id, rootState) {
      return service.completeRequest(id).then( data => {
        dispatch.requests.complete();
      });
    },
    async updateRequest(payload, rootState) {
      dispatch.requests.update(payload);
    },
  })
}
