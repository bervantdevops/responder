import React, {useState} from 'react';
import {View, Button, Text, Item, Input, Form, Picker, Label, Icon, H1} from 'native-base';
import {Helpers} from "my-components";
import OverlaySpinner from 'react-native-loading-spinner-overlay';

import styles from './contactStyles'

class ContactForm extends React.Component {
  state = {
    name: '',
    phone: '',
    relation: '',
  };

  submit = () => {
    const {name, phone, relation} = this.state;
    this.props.onSubmit({name, phone, relation})
  };

  render() {
    const {name, phone, relation} = this.state;
    const {errors, loading} = this.props;
    return (
      <Form style={styles.contactFormContainer}>
        <OverlaySpinner
          visible={loading}
          textContent={'Loading...'}
          textStyle={{ color: '#FFF'}}/>

        <H1>Add New Contact</H1>
        <Item floatingLabel input error={Helpers.hasError(errors, 'name')}>
          <Label>Contact Name</Label>
          <Input
            autoCapitalize="none"
            autoCorrect={false}
            returnKeyType="next"
            value={name}
            onChangeText={(name) => this.setState({name})}/>
          {Helpers.hasError(errors, 'name') && <Icon name='close-circle'/>}
        </Item>
        {Helpers.renderError(errors, 'name')}
        <Item floatingLabel input error={Helpers.hasError(errors, 'phone')}>
          <Label>Contact Phone No.</Label>
          <Input
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType="phone-pad"
            returnKeyType="next"
            value={phone}
            onChangeText={(phone) => this.setState({phone})}/>
          {Helpers.hasError(errors, 'phone') && <Icon name='close-circle'/>}
        </Item>
        {Helpers.renderError(errors, 'phone')}
        <Item inlineLabel={true} picker input error={Helpers.hasError(errors, 'relation')}>
          <Label>Relation</Label>
          <Picker
            mode="dropdown"
            iosIcon={<Icon name="arrow-down"/>}
            placeholder="Skill"
            placeholderStyle={{color: "#bfc6ea"}}
            placeholderIconColor="#007aff"
            style={{width: null}}
            selectedValue={relation}
            onValueChange={(itemValue, itemIndex) =>
              this.setState({relation: itemValue})
            }>
            <Picker.Item label="Select" value=""/>
            <Picker.Item label="Parent" value="Parent"/>
            <Picker.Item label="Guardian" value="Guardian"/>
            <Picker.Item label="Sibling" value="Sibling"/>
            <Picker.Item label="Relative" value="Relative"/>
            <Picker.Item label="Friend" value="Friend"/>
          </Picker>
          {Helpers.hasError(errors, 'relation') && <Icon name='close-circle'/>}
        </Item>
        {Helpers.renderError(errors, 'relation')}
        <View style={styles.formActionFooter}>
          <Button primary onPress={this.submit}>
            <Text>Add Contact</Text>
          </Button>
        </View>
      </Form>
    );
  }
};

export default ContactForm;
