import React from 'react'
import {Platform, StyleSheet, Image} from 'react-native'
import {
  Container, Header, Left, Right, Title, Body, Content, View, Text,
  Button, Item, Icon, Spinner, Fab, List, ListItem, Form
} from 'native-base';
import styles from './contactStyles'
import Modal from 'react-native-modal'
import ContactForm from "./ContactForm";
import {Helpers} from "my-components";
import OverlaySpinner from 'react-native-loading-spinner-overlay';

export default class Contacts extends React.Component {

  static navigationOptions = {
    drawerLabel: 'My Contacts',
    drawerIcon: ({tintColor}) => (<Icon name={"contacts"}/>)
  };

  state = {
    loading: false,
    bSending: false,
    bRemoving: false,
    fErrors: {},
    refreshing: false,
    addModalVisible: false,
  };

  componentWillMount = () => {
    const { fetchContacts, contacts } = this.props;
    fetchContacts().then( data => {

    })
  };

  toggleAddModal = () => {
    const { addModalVisible } = this.state;
    this.setState({ addModalVisible: !addModalVisible })
  };

  onAddNewContact =  (contact) => {
    this.setState({ bSending: true });
    this.props.addContact(contact)
      .then(() => {
        this.setState({ addModalVisible: false })
      })
      .catch((ex) => {
        switch (ex.response.status) {
          case 422:
            console.log("422 ===> ", ex.response.data);
            this.setState({fErrors: ex.response.data.errors});
            break;
          case 430:
            Helpers.showToast({type: 'error', message: ex.response.data.message});
            break;
          case 500:
            Helpers.showToast({type: 'error', message: ex.response.data.message});
            break;
          default:
            console.log(ex);
            break;
        }
      })
      .finally(() => {
        this.setState({ bSending: false })
      })
  };


  onRemoveContact =  (item, index) => {
    this.setState({ bRemoving: true });
    this.props.removeContact(item, index)
      .then(() => {

      })
      .catch((ex) => {
        switch (ex.response.status) {
          case 430:
            Helpers.showToast({type: 'error', message: ex.response.data.message});
            break;
          case 500:
            Helpers.showToast({type: 'error', message: ex.response.data.message});
            break;
          default:
            console.log(ex);
            break;
        }
      })
      .finally(() => {
        this.setState({ bRemoving: false })
      })
  };

  render() {
    const { addModalVisible, fErrors, bSending, bRemoving } = this.state;
    const { contacts } = this.props;
    return (
      <Container style={styles.container}>
        <Header iosBarStyle="dark-content" androidStatusBarColor={'#F1F3F5'}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name='arrow-back'/>
            </Button>
          </Left>
          <Body>
          <Title>My Contacts</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <OverlaySpinner
            visible={bRemoving}
            textContent={'Removing contact...'}
            textStyle={{ color: '#FFF'}}/>

          {!!contacts && contacts.map( ({ name, id, phone, relation}, index) => (
            <List key={index}>
              <ListItem noIndent>
                <Body>
                <Text>{name}</Text>
                <Text note>{phone} ({relation})</Text>
                </Body>
                <Right>
                  <Button transparent={true} onPress={() => this.onRemoveContact(id, index)}>
                    <Icon ios="ios-trash" android={"md-trash"} />
                  </Button>
                </Right>
              </ListItem>
            </List>
          ))}
          <Modal
            isVisible={addModalVisible}
            onBackButtonPress={() => this.setState({ addModalVisible: false})}
            onBackdropPress={() => this.setState({ addModalVisible: false})}>
            <View style={styles.modalContent}>
              <ContactForm loading={bSending} errors={fErrors} onSubmit={this.onAddNewContact}/>
            </View>
          </Modal>

        </Content>
        <Fab
          direction="up"
          containerStyle={{ }}
          style={{ backgroundColor: '#5067FF' }}
          position="bottomRight"
          onPress={this.toggleAddModal}>
          <Icon ios="ios-add" android={"md-add"} />
        </Fab>
      </Container>
    )
  }
}

