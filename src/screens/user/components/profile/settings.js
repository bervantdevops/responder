import React from 'react'
import {ListView, ActivityIndicator, Image} from 'react-native'
import {
  Container, Header, Left, Right, Title, Body, Content, View, Text,
  Button, Item, Icon, Spinner, List, ListItem, Separator, Thumbnail
} from 'native-base';
import styles from './profileStyles'


export default class Settings extends React.Component {

  state = {
    dataSource: null,
    isLoading: true,
    isLoadingMore: false,
    _data: null,
    nextPage: '',
  };

  logout = async () => {
    await this.props.logout();
    this.props.navigation.navigate('Auth');
  };


  back = () => {
    this.props.navigation.goBack();
  };


  render() {
    const { user } = this.props;
    return (
      <Container style={styles.container}>
        <Header iosBarStyle="dark-content" androidStatusBarColor={'#F1F3F5'}>
          <Left>
            <Button transparent onPress={this.back}>
              <Icon ios='ios-arrow-back' android="md-arrow-back"/>
            </Button>
          </Left>
          <Body>
          <Title>Profile</Title>
          </Body>
          <Right/>
        </Header>
        <Content>
         <Separator bordered>
            <Text>Account</Text>
          </Separator>
          {!!user && <ListItem avatar noIndent noBorder>
              <Left>
                <Thumbnail  source={{ uri: user.avatarUrl }} />
              </Left>
              <Body>
                <Text>{user.name}</Text>
                <Text note>{user.phone}</Text>
                <Text note>{user.id_number}</Text>
              </Body>
              <Right />
            </ListItem>}
          <Separator bordered>
            <Text>Settings</Text>
          </Separator>
          <ListItem noIndent>
            <Text>Privacy policy</Text>
          </ListItem>
          <ListItem noIndent>
            <Text>Terms of Use</Text>
          </ListItem>
          <ListItem last onPress={this.logout}>
            <Text>Sign Out</Text>
          </ListItem>
        </Content>
      </Container>
    )
  }
}


