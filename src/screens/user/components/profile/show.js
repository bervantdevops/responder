import React, {Component} from "react";
import {
  View,
  StyleSheet,
  Image,
  Dimensions,
  FlatList
} from "react-native";

import {
  Container, Content, Icon, Header,
  Left, Body, Right, Segment, Button,
  Title, Spinner, List, Text
} from 'native-base'
import { Theme } from 'my-components'
var {height, width} = Dimensions.get('window');
import userService from '../../module/userService'


class UserProfile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeIndex: 0,
      user: null,
      ready: false,
      posts: [],
      sending: false
    }

  };

  componentDidMount = () => {
    this.getUserProfile();
  };

  getUserProfile = () => {
    const {item} = this.props.navigation.state.params;
    this.setState({ready: false});
    userService.getUserProfile(item.pk).then(data => {
      this.setState({
        user: data.data,
        posts: data.posts
      })
    }).catch(error => {
      console.error(error);
    }).finally(() => {
      this.setState({ready: true});
    })
  };

  segmentClicked(index) {
    this.setState({
      activeIndex: index
    })
  }

  checkActive = (index) => {
    if (this.state.activeIndex !== index) {
      return (
        {color: 'grey'}
      )
    }
    else {
      return (
        {}
      )
    }

  };

  back = () => {
    this.props.navigation.goBack();
  };

  followUnfollow = () => {
    const { user } = this.state;
    this.setState({ following: true });
    userService.followUser(user.pk).then( data => {
      this.setState({
        user: Object.assign(user, data.data)
      })
    }).catch((ex) => {
      console.log("Follow Exception :=> ", ex)
    }).finally(() => {
      this.setState({ following: false });
    })
  };


  render() {
    const {item} = this.props.navigation.state.params;
    const {user, ready} = this.state;
    return (
      <Container style={styles.container}>
        <Header iosBarStyle="dark-content" androidStatusBarColor={'#F1F3F5'} style={{paddingLeft: 10}}>
          <Left>
            <Button transparent onPress={this.back}>
              <Icon ios="ios-arrow-back" android="md-arrow-back"/>
            </Button>
          </Left>
          <Body>
            <Title>{item.username}</Title>
          </Body>
          <Right />
        </Header>
        {ready ? this.renderContent() : this.renderLoading()}
      </Container>
    );
  }

  renderLoading() {
    return (
      <Content contentContainerStyle={styles.loadingContainer}>
        <Spinner color='blue'/>
      </Content>
    );
  }

  renderUserActions(){
    const { user } = this.state;
    const me  = this.props.user;
    if(me.pk == user.pk){
      return (
        <View
          style={{flexDirection: 'row', paddingHorizontal: 5}}>

          <Button bordered dark
                  style={{ flex: 3, marginLeft: 10, justifyContent: 'center', height: 30 }}><Text>Edit Profile</Text>
          </Button>
          <Button bordered dark style={{
            flex: 1,
            height: 30,
            marginRight: 10, marginLeft: 5,
            justifyContent: 'center'
          }}>
            <Icon name="settings" style={{color: 'black', fontSize: 15}}/>
          </Button>
        </View>
      );
    }else{
      if (user.i_follow){
        return (
          <View
            style={{flexDirection: 'row', paddingHorizontal: 5}}>
            <Button bordered primary onPress={this.followUnfollow}
                    style={{flex: 3, marginLeft: 10, justifyContent: 'center', height: 30}}>
              <Text>Unfollow</Text>
            </Button>
          </View>
        )
      }else{
        return (
          <View
            style={{flexDirection: 'row', paddingHorizontal: 5}}>
            <Button bordered primary onPress={this.followUnfollow}
                    style={{flex: 3, marginLeft: 10, justifyContent: 'center', height: 30}}>
              {!!this.state.following && <Spinner color={Theme.brandPrimary}/>}
              <Text>{user.follows_me ? 'Follow Back' : 'Follow'}</Text>
            </Button>
          </View>
        )
      }
    }
  }

  renderContent() {
    const {item} = this.props.navigation.state.params;
    const {user} = this.state;
    return (
      <Content>

        <View style={{paddingTop: 10}}>
          <View style={{flexDirection: 'row'}}>
            <View
              style={{flex: 1, alignItems: 'center', justifyContent: 'flex-start'}}>
              <Image source={{uri: user.avatar_url}}
                     style={{width: 75, height: 75, borderRadius: 37.5}}/>

            </View>
            <View style={{flex: 3}}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  alignItems: 'flex-end'
                }}>
                <View style={{alignItems: 'center'}}>
                  <Text>{user.total_posts}</Text>
                  <Text style={{fontSize: 10, color: 'grey'}}>Posts</Text>
                </View>
                <View style={{alignItems: 'center'}}>
                  <Text>{user.total_followers}</Text>
                  <Text style={{fontSize: 10, color: 'grey'}}>Followers</Text>
                </View>
                <View style={{alignItems: 'center'}}>
                  <Text>{user.total_followings}</Text>
                  <Text style={{fontSize: 10, color: 'grey'}}>Following</Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', alignItems: 'flex-start', paddingTop: 10}}>
                {this.renderUserActions()}
              </View>{/**End edit profile**/}
            </View>
          </View>

          <View style={{paddingBottom: 10, paddingTop: 10}}>
            <View style={{paddingHorizontal: 10}}>
              <Text style={{fontWeight: 'bold'}}>{user.name}</Text>
            </View>
          </View>


        </View>


        <View>
          <View style={{
            flexDirection: 'row',
            justifyContent: 'space-around',
            borderTopWidth: 1,
            borderTopColor: '#eae5e5'
          }}>
            <Button

              onPress={() => this.segmentClicked(0)}
              transparent
              active={this.state.activeIndex == 0}

            >
              <Icon name="ios-apps-outline"
                    style={[this.state.activeIndex == 0 ? {} : {color: 'grey'}]}>
              </Icon>
            </Button>
            <Button
              onPress={() => this.segmentClicked(1)}
              transparent active={this.state.activeIndex == 1}>
              <Icon name="ios-list-outline"
                    style={[{fontSize: 32}, this.state.activeIndex == 1 ? {} : {color: 'grey'}]}/>
            </Button>
          </View>


        </View>
      </Content>
    );
  }
}

export default UserProfile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});