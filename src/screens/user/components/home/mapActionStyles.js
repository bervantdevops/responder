import {Dimensions} from 'react-native';

const {height, width} = Dimensions.get("window");

export default {
  actionsContainer: {
    position: 'relative',
    top: height * 0.4,
    bottom: height * 0.1,
    marginHorizontal: 10,
    backgroundColor: 'transparent',
    width: "80%",
  },
  currentRequestContainer: {
    position: 'relative',
    top: height * 0.4,
    bottom: height * 0.1,
    backgroundColor: '#fff',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    width: width * 0.8,
    marginRight: 15,
    elevation: 4,
    shadowColor: '#CCC',
    shadowOpacity: 1,
    shadowRadius: 5,
    shadowOffset: {width: 0, height: 3}
  },
  searchContent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10
  },
  bodyContent: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginHorizontal: 20,
  },
  menuBox: {
    flexDirection: 'column',
    width: '30%',
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 7,
    borderBottomWidth: 0,
    backgroundColor: '#FFF',
    borderColor: 'rgba(0,0,0,0.1)',
    elevation: 4,
    shadowColor: '#000',
    shadowOpacity: 1,
    shadowRadius: 5,
    shadowOffset: {width: 0, height: 3}
  },
  icon: {
    width: 70,
    height: 70,
  },
  info: {
    marginTop: 10,
    fontSize: 15,
  }

}
