import React from 'react';
import { Image } from 'react-native';
import { View, Text, Spinner, Button, Item, Thumbnail } from 'native-base';
import styles from './mapActionStyles'
import { MyStyles } from 'my-components'
import { UserCard } from '../../../../components';


export default class MapActions extends React.Component {
  getCategoryFromRequest = (request) => {
    return categories.filter((b) => b.name == request.category)[0];
  };

  render() {
    const { onCancelRequest, currentRequest } = this.props;

    return (
      <View style={styles.currentRequestContainer}>
        <View style={styles.searchContent}>
          <UserCard user={currentRequest.user} />
          {this.renderAction()}
        </View>
      </View>
    );
  }

  renderAction() {
    const { onCancelRequest, onStart, currentRequest, onComplete } = this.props;
    switch (currentRequest.status) {
      case 'dispatched':
        return (
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Button iconRight block style={MyStyles.Styles.bgPrimary} onPress={() => onStart(currentRequest.hashId)}>
              <Text style={MyStyles.Styles.textWhite}>Start Evacuation</Text>
            </Button>
            <Button dark block style={{ marginLeft: 5 }}
              onPress={() => onCancelRequest(currentRequest.hashId)}>
              <Text style={MyStyles.Styles.textWhite}>Cancel</Text>
            </Button>
          </View>
        );
      case 'evacuating':
        return (
          <View style={{ flex: 1 }}>

            <Text style={MyStyles.Styles.textWhite}>Evacuation in progress</Text>
            <Button iconRight block style={MyStyles.Styles.bgPrimary} onPress={() => onComplete(currentRequest.hashId)}>
              <Text>Complete Request</Text>
            </Button>

          </View>
        );
    }
  }
}
