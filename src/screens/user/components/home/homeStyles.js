import { StyleSheet } from 'react-native'
import { Constants } from 'expo';
const styles = {
  container: {
    backgroundColor: '#F1F3F5',
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end'
  },
  mapContainer: {
    flex: 1
  },
  btnBack: {
    position: 'absolute',
    top: 20,
    left: 20
  },
  headerText: {
    marginTop: 30,
    fontSize: 20,
    fontWeight: 'bold',
    alignSelf: 'center'
  },
  headerContainer: {
    flex: 5,
    position: 'relative',
    padding: 20,
    justifyContent: 'center'
  },
  footer: {
    flex: 2,
    padding: 20,
    flexDirection: 'column',
    backgroundColor: 'transparent',
    zIndex: -1,
  },
  btnSignUp: {
    marginVertical: 10
  },
  btnSignIn: {
    marginVertical: 10
  },
  map: {
    flex: 1,
    marginTop: 1.5,
    ...StyleSheet.absoluteFillObject,
  },
};

export default styles;