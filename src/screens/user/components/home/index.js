import React from 'react'
import {Platform, StyleSheet, Image, Switch, Alert} from 'react-native'
import {
  Container, Header, Left, Right, Title, Body, Content, View, Text,
  Button, Item, Icon, Spinner, Fab, List
} from 'native-base';
import MapViewDirections from 'react-native-maps-directions';
import styles from './homeStyles'
import mapStyle from './mapStyles'
import userService from '../../module/userService'

import {Notifications, Permissions, Location, Constants, MapView} from 'expo'
import {Helpers} from "my-components";
import CurrentRequestCard from "./CurrentRequestCard";
import OverlaySpinner from 'react-native-loading-spinner-overlay';
import * as helpers from '../../../../libs/helpers'
import Pusher from 'pusher-js/react-native'

const myLocation = require('../../../../assets/images/location-pin.png');
const mapKey = 'AIzaSyBhIEqmRDfLgXu51IzExZDC5A2kh6xwBic';

Pusher.logToConsole = true;

// var pusher = new Pusher('fd8ca7b100796ecda538', {
//   cluster: 'ap2',
//   forceTLS: true
// });

export default class Home extends React.Component {

  static navigationOptions = {
    drawerLabel: 'Home',
    drawerIcon: ({tintColor}) => (<Icon name={"home"}/>)
  };

  state = {
    loading: false,
    location: null,
    errorMessage: null,
    error: null,
    refreshing: false,
    bRequesting: false,
    bCancelling: false,
    bAccepting: false,
    bCompleting: false,
  };

  avaialbe_responders_channel = null;
  my_channel = null;
  request_channel = null;
  pusher = null;

  constructor(props) {
    super(props);

    console.ignoredYellowBox = [
      'Setting a timer'
    ];
  }

  componentDidMount = () => {
    const {currentRequest} = this.props;
    console.log({currentRequest})
  };

  componentWillMount() {
    if (Platform.OS === 'android' && !Constants.isDevice) {
      this.setState({
        errorMessage: 'Oops, this will not work on Sketch in an Android emulator. Try it on your device!',
      });
    } else {
      this._getLocationAsync();
    }
    const { user } = this.props;

    this.pusher = helpers.pusher();
    this.my_channel = this.pusher.subscribe(`Responder.${user.id}`);

    this.my_channel.bind('emergency.dispatched', ({ emergency }) => {

      if(!!this.props.currentRequest){
        return;
      }

      Alert.alert(
        "New request",
        "User: " + emergency.user.name + " need helps",
        [
          {
            text: "Ignore",
            onPress: () => {
              console.log('Cancel Pressed');
            },
            style: 'cancel'
          },
          {
            text: 'Respond!',
            onPress: () => this._acceptRequest(emergency.hashId)
          },
        ],
        { cancelable: false }
      );
    })

    this.my_channel.bind('emergency.cancelled', ({ emergency }) => {
      Alert.alert(
        "Cancellation",
        "Sorry it appears the request was cancelled",
        [
          {
            text: 'Okay',
            onPress: () => this.props.updateRequest(null)
          },
        ],
        { cancelable: false }
      );
    })
  }

  componentWillUnmount() {
    this._notificationSubscription && this
      ._notificationSubscription
      .remove();
  }

  _getLocationAsync = async () => {
    let {status} = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        locationPermission: false,
        errorMessage: 'Permission to access location was denied',
      });
    }

    let location = await Location.getCurrentPositionAsync({});
    location.region = Helpers.regionFrom(location.coords);
    this.setState({location});
  };

  toggleOnlineStatus = () => {

    const {location} = this.state;
    this.setState({bRequesting: true});

    let {changeOnlineStatus, user} = this.props;
    changeOnlineStatus({
      online: !user.online,
      latitude: location.region.latitude,
      longitude: location.region.longitude
    }).then(() => {

    }).catch((ex) => {
      switch (ex.response.status) {
        case 422:
          console.log("422 ===> ", ex.response.data);
          Helpers.showToast({type: 'error', message: ex.response.data.message});
          break;
        case 430:
          Helpers.showToast({type: 'error', message: ex.response.data.message});
          break;
        case 500:
          Helpers.showToast({type: 'error', message: ex.response.data.message});
          break;
        default:
          console.log(ex);
          break;
      }
    })
      .finally(() => {
        this.setState({bRequesting: false});
      });
  };

  _cancelRequest = (request_id) => {
    this.setState({bCancelling: true});
    console.log({request_id});
    this.props.cancelRequest(request_id)
      .then(() => {

      })
      .catch((ex) => {
        switch (ex.response.status) {
          case 422:
            console.log("422 ===> ", ex.response.data);
            Helpers.showToast({type: 'error', message: ex.response.data.message});
            break;
          case 430:
            Helpers.showToast({type: 'error', message: ex.response.data.message});
            break;
          case 500:
            Helpers.showToast({type: 'error', message: ex.response.data.message});
            break;
          default:
            console.log(ex);
            break;
        }
      })
      .finally(() => {
        this.setState({bCancelling: false});
      });
  };

  _acceptRequest = (request_id) => {
    this.setState({bAccepting: true});

    this.props.acceptRequest(request_id)
      .then(() => {

      })
      .catch((ex) => {
        switch (ex.response.status) {
          case 422:
            console.log("422 ===> ", ex.response.data);
            Helpers.showToast({type: 'error', message: ex.response.data.message});
            break;
          case 430:
            Helpers.showToast({type: 'error', message: ex.response.data.message});
            break;
          case 500:
            Helpers.showToast({type: 'error', message: ex.response.data.message});
            break;
          default:
            console.log(ex);
            break;
        }
      })
      .finally(() => {
        this.setState({bAccepting: false});
      });
  };

  _startEvacuation = (request_id) => {
    this.setState({bAccepting: true});
    this.props.startRequest(request_id)
      .then(() => {

      })
      .catch((ex) => {
        switch (ex.response.status) {
          case 422:
            console.log("422 ===> ", ex.response.data);
            Helpers.showToast({type: 'error', message: ex.response.data.message});
            break;
          case 430:
            Helpers.showToast({type: 'error', message: ex.response.data.message});
            break;
          case 500:
            Helpers.showToast({type: 'error', message: ex.response.data.message});
            break;
          default:
            console.log(ex);
            break;
        }
      })
      .finally(() => {
        this.setState({bAccepting: false});
      });
  };

  _completeRequest = (request_id) => {
    this.setState({bCompleting: true});
    this.props.completeRequest(request_id)
      .then(() => {

      })
      .catch((ex) => {
        switch (ex.response.status) {
          case 422:
            console.log("422 ===> ", ex.response.data);
            Helpers.showToast({type: 'error', message: ex.response.data.message});
            break;
          case 430:
            Helpers.showToast({type: 'error', message: ex.response.data.message});
            break;
          case 500:
            Helpers.showToast({type: 'error', message: ex.response.data.message});
            break;
          default:
            console.log(ex);
            break;
        }
      })
      .finally(() => {
        this.setState({bCompleting: false});
      });
  };

  render() {
    const {location, bRequesting, bCancelling, bAccepting, bCompleting} = this.state;
    const {user} = this.props;
    return (
      <Container style={styles.container}>
        <Header iosBarStyle="dark-content" androidStatusBarColor={'#F1F3F5'}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.openDrawer()}>
              <Icon name='menu'/>
            </Button>
          </Left>
          <Body>
          <Title>Home</Title>
          </Body>
          <Right>
            <Switch value={!!user && !!user.online} onValueChange={this.toggleOnlineStatus}/>
          </Right>
        </Header>
        <OverlaySpinner
          visible={bCancelling || bRequesting || bAccepting || bCompleting}
          textContent={'Loading ....'}
          textStyle={{color: '#FFF'}}/>
        <View style={{flex: 1}}>
          {!!location && this._renderMap()}
          {!location && this._renderSpinner()}
        </View>
      </Container>
    )
  }

  _renderSpinner = () => {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Spinner color={'blue'}/>
        <Text>Getting things ready. One minute ...</Text>
      </View>
    )
  };

  _renderMap = () => {
    const {location} = this.state;
    const {currentRequest} = this.props;
    return (
      <View style={styles.mapContainer}>
        <MapView style={styles.map}
                 initialRegion={location.region}
                 provider={"google"}
                 loadingEnabled={true}
                 customMapStyle={mapStyle}
                 showsUserLocation={false}>
          {/*{!!currentRequest && !Helpers.status(currentRequest, ["working","complete"])  && <MapViewDirections*/}
          {/*  origin={location.coords}*/}
          {/*  destination={location.coords}*/}
          {/*  apikey={mapKey}*/}
          {/*/>}*/}
          <MapView.Marker
            key={0}
            coordinate={location.coords}>
            <Image source={myLocation} style={{height: 40, width: 40}}/>
          </MapView.Marker>
        </MapView>
        <View style={{position: 'absolute', top: 100, left: 50}}>
          {!!currentRequest && (
            <CurrentRequestCard
            currentRequest={currentRequest}
            onCancelRequest={this._cancelRequest}
            onStart={this._startEvacuation}
            onComplete={this._completeRequest}
            onPressItem={this.makeRequest}/>
          )}
        </View>
      </View>

    )
  };


  async registerForPushNotificationsAsync() {
    const {status: existingStatus} = await Permissions.getAsync(
      Permissions.NOTIFICATIONS
    );
    let finalStatus = existingStatus;

    // only ask if permissions have not already been determined, because
    // iOS won't necessarily prompt the user a second time.
    if (existingStatus !== 'granted') {
      // Android remote notification permissions are granted during the app
      // install, so this will only ask on iOS
      const {status} = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }

    // Stop here if the user did not grant permissions
    if (finalStatus !== 'granted') {
      return;
    }
    let token = await Notifications.getExpoPushTokenAsync();

    // const user = await helpers.getItemFromState('user');
    //
    // // Get the token that uniquely identifies this device
    //
    //
    // if (user.push_token && user.push_token == token)
    //   return;

    return userService.registerPushToken(token)
      .then(data => {

      })
  }

  _handleNotification = (notification) => {
    this.setState({notification: notification});
    // console.log("Notification: ", notification)
  };
}

