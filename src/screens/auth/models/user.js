import authService from '../module/authService'
import userService from '../../user/module/userService';

export default {
  state: null, // initial state
  reducers: {
    // handle state changes with pure functions
    set(state, payload) {
      return payload;
    },
    addDetails(state, payload) {
      return payload;
    }
  },
  effects: (dispatch) => ({
    async login({username, password}, rootState) {
      console.log(username);
      return authService.auth(username, password).then( data => {
        const user  = Object.assign(data.data, { accessToken: data.accessToken});
        dispatch.user.set(user);
      });
    },
    async fbLogin({token}, rootState) {
      return authService.fbLogin(token).then( data => {
        const user  = Object.assign(data.data, { accessToken: data.accessToken});
        dispatch.user.set(user);
      });
    },
    async logout(payload, rootState) {
      return userService.logout().then( data => {
        dispatch.user.set(null);
        dispatch.requests.clear(nll);
      });
    },
    async refresh(payload, rootState){
      return userService.myProfile().then( data => {
        const newState = Object.assign(rootState.user, data.data);
        dispatch.user.set(newState);
        dispatch.requests.update(data.currentRequest);
      });
    },
    async updateProfile(payload, rootState){
      return userService.updateProfile(payload).then( data => {
        const newState = Object.assign(rootState.user, data.data);
        dispatch.user.set(newState);
      });
    },
    async onlineStatus(payload, rootState){
      return userService.toggleOnlineStatus(payload).then( data => {
        const newState = Object.assign(rootState.user, data.data);
        dispatch.user.set(newState);
      });
    },
    async logout(){
      dispatch.user.set(null)
    },
  })
}
