import * as helpers from '../../../libs/helpers'
import axios from 'axios'
import {Helpers} from "my-components";

export default {
  auth: async (username, password) => {
    return axios.post(helpers.url('member/login'), {
      username: username,
      password: password
    }).then(resp => {
      return resp.data;
    })
  },
  sendVerification: async (params) => {
    return axios.post(helpers.url('auth/send-verification-code'), params).then(resp => {
      return resp.data;
    })
  },
  verifyCode: async (params) => {
    console.log({params})
    return axios.post(helpers.url('auth/verify-code'), params).then(resp => {
      return resp.data;
    })
  },
  changePassword: async (params) => {
    return axios.post(helpers.url('member/reset-password'), params).then(resp => {
      return resp.data;
    })
  },
}
