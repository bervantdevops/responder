import {connect} from "react-redux";
import { bindActionCreators } from 'redux'
import  * as authActions from "../module/actions";
import Welcome from '../components/welcome';

const mapStateToProps = state => ({
  user: state.user
});

const  mapDispatchToProps = ({ user: { refresh }}) => ({
  myProfile: () => refresh()
});

export default connect(mapStateToProps, mapDispatchToProps)(Welcome);