import {connect} from "react-redux";
import { bindActionCreators } from 'redux'
import  * as authActions from "../module/actions";
import Register from '../components/register';

const mapStateToProps = state => ({
  user: state.user,
  models: state.models
});

const mapDispatchToProps = dispatch => ({
  register: (params) => dispatch.user.register(params),
  fbLogin: (token) => dispatch.user.fbLogin({ token }),
});


export default connect(mapStateToProps, mapDispatchToProps)(Register);