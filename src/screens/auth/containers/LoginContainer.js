import {connect} from "react-redux";
import { bindActionCreators } from 'redux'
import  * as authActions from "../module/actions";
import Login from '../components/login';

const mapStateToProps = state => ({
  user: state.user,
  models: state.models
});

const mapDispatchToProps = dispatch => ({
  login: (username, password) => dispatch.user.login({ username, password}),
  fbLogin: (token) => dispatch.user.fbLogin({ token }),
});
export default connect(mapStateToProps, mapDispatchToProps)(Login);