import {connect} from "react-redux";
import UpdatePhone from '../components/phone_number';

const mapStateToProps = state => ({
  user: state.user
});

const mapDispatchToProps = ({ user: { updateProfile }}) => ({
  profile: (payload) => updateProfile(payload),
});
export default connect(mapStateToProps, mapDispatchToProps)(UpdatePhone);