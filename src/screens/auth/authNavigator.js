import {Platform} from "react-native";
import { createStackNavigator } from "react-navigation";
import {Login, Register, Welcome, UpdatePhone, ForgotPassword} from './index'

export default createStackNavigator({
  Welcome: Welcome,
  Login: Login,
  Register: Register,
  UpdatePhone: UpdatePhone,
  ForgotPassword: ForgotPassword,
}, {
  initialRouteName: 'Welcome',
  headerMode: 'none',
  navigationOptions: () => ({
    header: null
  })
});
