import Login from './containers/LoginContainer'
import Register from './containers/RegisterContainer'
import Welcome from './containers/WelcomeContainer'
import UpdatePhone from './containers/UpdatePhoneContainer'
import ForgotPassword from './containers/ForgotPasswordContainer'

module.exports =  {
  Login, Register, Welcome, UpdatePhone, ForgotPassword
};
