import React from 'react'
import {
  Container,
  View,
  Text,
  Button,
  Form,
  Item,
  Label,
  Input,
  Icon,
  Left,
  Body,
  Title,
  Right,
  Header,
  Picker,
  Content,
  Switch
} from 'native-base';
import DatePicker from 'react-native-datepicker';
import {Grid, Col} from 'react-native-easy-grid'
import styles from './registerStyles';
import {Loader, Helpers, Config} from 'my-components';
import authService from '../../module/authService'

export default class Register extends React.Component {
  state = {
    name: '',
    dob: null,
    phone: '',
    gender: '',
    password: '',
    locality_id: '',
    sending: false,
    errors: {},
    counties: [],
    showPassword: false,
    volunteer: false,
    volunteer_category: '',
  };

  async componentWillMount() {
    authService.getCounties().then( ({ data}) => {
      this.setState({ counties: data})
    })
  }

  back = () => {
    return this.props.navigation.goBack();
  };

  submit = () => {
    this.setState({
      sending: true
    });
    const {dob, name, gender, password, phone, locality_id, volunteer, volunteer_category} = this.state;
    this.props.register({name, dob, phone, gender, password, locality_id, volunteer_category, volunteer}).then(() => {

    }).catch((ex) => {
      console.log(ex.message, ex);
      switch (ex.response.status) {
        case 422:
          console.log("422 ===> ", ex.response.data);
          this.setState({errors: ex.response.data.errors});
          break;
        case 430:
          Helpers.showToast({type: 'error', message: ex.response.data.message});
          break;
        case 500:
          Helpers.showToast({type: 'error', message: ex.response.data.message});
          break;
        default:
          console.log(ex);
          break;
      }
    }).finally(() => {
      this.setState({
        sending: false
      });

      const {user} = this.props
      if (!!user) {
        if (!user.phone)
          return this.props.navigation.navigate('UpdatePhone');

        return this.props.navigation.navigate('App');
      }
    })
  };

  render() {
    const {errors, name, dob, gender, password, phone, counties, locality_id, showPassword, volunteer, volunteer_category} = this.state;
    return (
      <Container style={styles.container}>
        <Header iosBarStyle="dark-content" androidStatusBarColor={'#F1F3F5'}>
          <Left>
            <Button transparent onPress={this.back}>
              <Icon name='menu' android="md-arrow-back" ios="ios-arrow-back"/>
            </Button>
          </Left>
          <Body>
          <Title>Register</Title>
          </Body>
          <Right/>
        </Header>
        <Loader
          loading={this.state.sending}/>
        <Content padder>
          <Form>
            <Item floatingLabel input error={Helpers.hasError(errors, 'name')}>
              <Label>Name</Label>
              <Input
                value={name}
                autoCapitalize="none"
                autoCorrect={false}
                onChangeText={(name) => this.setState({name})}/>
              {Helpers.hasError(errors, 'name') && <Icon name='close-circle'/>}
            </Item>
            {Helpers.renderError(errors, 'name')}
            <Item picker input error={Helpers.hasError(errors, 'gender')}>
              <Label>Gender</Label>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                placeholder="Gender"
                placeholderStyle={{ color: "#bfc6ea" }}
                placeholderIconColor="#007aff"
                style={{ width: null }}
                selectedValue={gender}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({gender: itemValue})
                }>
                <Picker.Item label="Select Gender" value="" />
                <Picker.Item label="Female" value="Female" />
                <Picker.Item label="Male" value="Male" />
              </Picker>
            </Item>
            {Helpers.renderError(errors, 'gender')}
            <Item picker input error={Helpers.hasError(errors, 'locality_id')}>
              <Label>County</Label>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                placeholder="Gender"
                placeholderStyle={{ color: "#bfc6ea" }}
                placeholderIconColor="#007aff"
                style={{ width: null }}
                selectedValue={locality_id}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({locality_id: itemValue})
                }>
                <Picker.Item label="Select County" value="" />
                {!!counties && counties.map((item, index) =>  (<Picker.Item key={index} label={item.name} value={item.id} />))}
              </Picker>
            </Item>
            {Helpers.renderError(errors, 'locality_id')}
            <Item floatingLabel input error={Helpers.hasError(errors, 'phone')}>
              <Label>Phone Number</Label>
              <Input
                value={phone}
                keyboardType="phone-pad"
                onChangeText={(phone) => this.setState({phone})}/>
              {Helpers.hasError(errors, 'phone') && <Icon name='close-circle'/>}
            </Item>
            {Helpers.renderError(errors, 'phone')}
            <Grid style={{ marginTop: 30 }}>
              <Col>
                <Text>Become a volunteer?</Text>
              </Col>
              <Col>
                <Switch value={volunteer} onValueChange={(newValue) => this.setState({ volunteer: newValue})} />
              </Col>
            </Grid>
            {Helpers.renderError(errors, 'volunteer')}
            {!!volunteer &&  <Item picker input error={Helpers.hasError(errors, 'volunteer_category')}>
              <Label>Volunteer Skill</Label>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                placeholder="Skill"
                placeholderStyle={{ color: "#bfc6ea" }}
                placeholderIconColor="#007aff"
                style={{ width: null }}
                selectedValue={volunteer_category}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({volunteer_category: itemValue})
                }>
                <Picker.Item label="Select" value="" />
                <Picker.Item label="Gun Owner" value="GunOwner" />
                <Picker.Item label="Medical Expert" value="Medical" />
                <Picker.Item label="Other" value="Other" />
              </Picker>
            </Item>}
            {Helpers.renderError(errors, 'volunteer_category')}
            <DatePicker
              style={{width: '100%', marginTop: 30}}
              date={dob}
              mode="date"
              placeholder="Date of Birth"
              format="YYYY-MM-DD"
              minDate="1940-01-01"
              maxDate="2016-06-01"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  left: 0,
                  top: 4,
                  marginLeft: 0
                },
                dateInput: {
                  marginLeft: 36,
                  borderTopWidth: 0,
                  borderLeftWidth: 0,
                  borderRightWidth: 0,
                  textAlign: 'left'
                }
                // ... You can check the source to find the other keys.
              }}
              onDateChange={(dob) => {this.setState({ dob })}}
            />
            {Helpers.renderError(errors, 'dob')}
            <Item input error={Helpers.hasError(errors, 'password')}>
              <Input
                placeholder={"Password"}
                autoCapitalize="none"
                autoCorrect={false}
                secureTextEntry={!showPassword}
                value={password}
                onChangeText={(password) => this.setState({password})}/>
              <Icon name='lock' onPress={() => this.setState({ showPassword: !showPassword})} />
            </Item>
            {Helpers.renderError(errors, 'password')}
          </Form>
          <View style={styles.footer}>
            <Button danger block style={styles.btnSignIn} onPress={this.submit}>
              <Text>Continue</Text>
            </Button>
          </View>
        </Content>
      </Container>
    )
  }
}

