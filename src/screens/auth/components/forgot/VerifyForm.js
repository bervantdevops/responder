import React from 'react'
import {
  Container,
  View,
  Text,
  Button,
  Form,
  Item,
  Label,
  Input,
  Icon,
  Left,
  Body,
  Title,
  Right,
  Header
} from 'native-base';
import styles from './forgotStyles'
import { Loader, Helpers, Config } from 'my-components';
import CodeInput from 'react-native-confirmation-code-input';

export default class VerifyForm extends React.Component {
  state = {
    code: '',
  };

  componentDidMount = () => {

  };

  back = () => {
    return this.props.navigation.goBack();
  };

  submit = (code) => {
    const {form, onSubmit} = this.props;
    this.props.onSubmit(code);
  };




  render() {
    const { code } =  this.state;
    return (
      <View  style={styles.container}>
        <View style={styles.headerContainer}>
          <Text style={styles.headerText}>Verify Code</Text>

           <CodeInput
            ref="codeInputRef2"
            secureTextEntry={false}
            activeColor='rgba(49, 180, 4, 1)'
            inactiveColor='rgba(49, 180, 4, 1.3)'
            autoFocus={false}
            ignoreCase={true}
            inputPosition='center'
            size={50}
            onFulfill={(code) => this.submit(code)}
            containerStyle={{ marginTop: 30 }}
            codeInputStyle={{ borderWidth: 1.5 }}
          />
        </View>
      </View>
    )
  }
}

