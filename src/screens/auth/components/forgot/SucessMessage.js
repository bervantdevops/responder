import React from 'react'
import {
  Container,
  View,
  Text,
  Button,
  Form,
  Item,
  Label,
  Input,
  Icon,
  Left,
  Body,
  Title,
  Right,
  Header
} from 'native-base';
import styles from './forgotStyles'
import { Loader, Helpers, Config } from 'my-components'

export default class SuccessMessage extends React.Component {
  state = {

  };

  componentDidMount = () => {

  };

  back = () => {
    return this.props.navigation.goBack();
  };

  submit = () => {
    this.props.onSubmit()
  };


  render() {
    const {phone} =  this.state;
    const {errors} = this.props
    return (
      <View  style={styles.container}>
        <View style={istyles.messageContainer}>
          <Icon
          style={istyles.messageIcon}
          ios="ios-checkmark-circle-outline"
           android="md-checkmark-circle-outline" />
          <Text style={istyles.messageText}>Aye aye captain. You have successfully reset your password</Text>

        </View>
        <View style={styles.footer}>
          <Button primary block  onPress={this.submit}>
            <Text>Login</Text>
          </Button>
        </View>

      </View>
    )
  }
}

const istyles = {
  messageContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  messageIcon: {
    fontSize: 80,
    color: '#00A82B'
  },
  messageText: {
    fontSize: 17,
    textAlign: 'center',
    color: '#00A82B',
    marginTop: 20,
  }
}

