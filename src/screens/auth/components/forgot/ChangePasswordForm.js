import React from 'react'
import {
  Container,
  View,
  Text,
  Button,
  Form,
  Item,
  Label,
  Input,
  Icon,
  Left,
  Body,
  Title,
  Right,
  Header
} from 'native-base';
import styles from './forgotStyles'
import { Loader, Helpers, Config } from 'my-components'

export default class ChangePasswordForm extends React.Component {
  state = {
    password: '',
    showPassword: false,
  };

  componentDidMount = () => {

  };

  back = () => {
    return this.props.navigation.goBack();
  };

  submit = () => {
    const {password} = this.state;
    this.props.onSubmit(password)
  };


  render() {
    const { showPassword, password} =  this.state;
    const { errors, form} =  this.props;
    return (
      <View  style={styles.container}>
        <View style={styles.headerContainer}>
          <Text style={styles.headerText}>Phewks!! Finally, lets change your password.</Text>
          <Form>
          <Item input error={Helpers.hasError(errors, 'password')}>
              <Input
                placeholder={"Password"}
                autoCapitalize="none"
                autoCorrect={false}
                secureTextEntry={!showPassword}
                value={password}
                onChangeText={(password) => this.setState({password})}/>
              <Icon ios={showPassword ? 'ios-eye-off' : 'ios-eye'}
                  android={showPassword ? 'md-eye-off' : 'md-eye'}
                  onPress={() => this.setState({ showPassword: !showPassword})} />
            </Item>
            {Helpers.renderError(errors, 'password')}
          </Form>
        </View>
        <View style={styles.footer}>
          <Button danger block style={styles.btnSignIn} onPress={this.submit}>
            <Text>Change Password</Text>
          </Button>
        </View>

      </View>
    )
  }
}

