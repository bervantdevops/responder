import React from 'react'
import {
  Container,
  View,
  Text,
  Button,
  Form,
  Item,
  Label,
  Input,
  Icon,
  Left,
  Body,
  Title,
  Right,
  Header
} from 'native-base';
import styles from './forgotStyles'
import { Loader, Helpers, Config } from 'my-components'

export default class ForgotForm extends React.Component {
  state = {
    phone: '',
    sending: false,
    errors: {},
    title: "Forgot Password",
    step: 'forgot'
  };

  componentDidMount = () => {

  };

  back = () => {
    return this.props.navigation.goBack();
  };

  submit = () => {
    this.props.onSubmit(this.state.phone)
  };


  render() {
    const {phone} =  this.state;
    const {errors} = this.props
    return (
      <View  style={styles.container}>
        <View style={styles.headerContainer}>
          <Text>Forgot you password? Don't worry we all forget, I guess. I will help you get your account back in a few steps</Text>
          <Form>
            <Item floatingLabel input error={Helpers.hasError(errors, 'phone')}>
              <Label>Phone No.</Label>
              <Input
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="phone-pad"
                returnKeyType="next"
                value={phone}
                onChangeText={(phone) => this.setState({phone})}/>
              {Helpers.hasError(errors, 'phone') && <Icon name='close-circle' />}
            </Item>
            {Helpers.renderError(errors, 'phone')}

          </Form>
        </View>
        <View style={styles.footer}>
          <Button danger block style={styles.btnSignIn} onPress={this.submit}>
            <Text>Send Code</Text>
          </Button>
        </View>

      </View>
    )
  }
}

