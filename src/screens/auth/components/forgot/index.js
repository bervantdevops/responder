import React from 'react'
import {
  Container,
  View,
  Text,
  Button,
  Form,
  Item,
  Label,
  Input,
  Icon,
  Left,
  Body,
  Title,
  Right,
  Header
} from 'native-base';
import styles from './forgotStyles'
import { Loader, Helpers, Config } from 'my-components'
import ForgotForm from './ForgotForm';
import ChangePasswordForm from './ChangePasswordForm';
import SuccessMessage from './SucessMessage';
import VerifyForm from './VerifyForm';
import authService from '../../module/authService'

export default class Forgot extends React.Component {
  state = {
    username: '',
    password: '',
    sending: false,
    errors: {},
    title: "Forgot Password",
    step: 'forgot'
  };

  componentDidMount = () => {

  };

  back = () => {
    const {step} = this.state;
    switch(step){
      case 'reset':
        return this.setState({ step: 'verify'})
      case 'verify':
        return this.setState({ step: 'forgot'})
      default:
          return this.props.navigation.goBack();
    }

  };

  sendCode = (phone) => {
    this.setState({
      sending: true,
      phone: phone,
    });
    const {username, password}  = this.state;
    authService.sendVerification({phone: phone}).then(() => {
      this.setState({ step: "verify"})
    }).catch((ex) => {
      this.processError(ex)
    }).finally(() => {
      this.setState({
        sending: false,
      });
    })
  };

  verifyCode = (code) => {
    this.setState({
      sending: true,
      code: code
    });

    authService.verifyCode({phone: this.state.phone, code: code}).then(() => {
      this.setState({ step: "reset"})
    }).catch((ex) => {
      this.processError(ex)
    }).finally(() => {
      this.setState({
        sending: false,
      });
    })
  };

  changePassword = (password) => {
    this.setState({
      sending: true,
      password: password
    });
    const {phone, code} = this.state;

    authService.changePassword({phone: phone, code: code, password: password}).then(() => {
      this.setState({ step: "done"})
    }).catch((ex) => {
      this.processError(ex)
    }).finally(() => {
      this.setState({
        sending: false,
      });
    })
  };

  onLogin = () => {
    this.props.navigation.navigate('Login')
  }

  processError = (ex) => {
    if(!ex.response){
      Helpers.showToast({type: 'error', message: ex.message});
      return;
    }

    console.log(ex.message, ex);
    switch (ex.response.status) {
      case 422:
        this.setState({errors: ex.response.data.errors});
        Helpers.showToast({type: 'error', message: "Invalid username or password"});
        break;
      case 430:
        Helpers.showToast({type: 'error', message: ex.response.data.message});
        break;
      case 500:
        Helpers.showToast({type: 'error', message: ex.response.data.message});
        break;
      default:
        console.log(ex);
        break;
    }
  }


  render() {
    const { errors, title, step} =  this.state;
    return (
      <Container  style={styles.container}>
        <Header iosBarStyle="dark-content" androidStatusBarColor={'#F1F3F5'}>
          <Left>
            <Button transparent onPress={this.back}>
              <Icon name='menu' android="md-arrow-back" ios="ios-arrow-back"/>
            </Button>
          </Left>
          <Body>
          <Title>{ title }</Title>
          </Body>
          <Right/>
        </Header>
        <Loader
          loading={this.state.sending} />
          {this.renderForm()}
      </Container>
    )
  }

  renderForm() {
    const { errors, title, step} =  this.state;

    switch(step) {
      case 'done':
          return <SuccessMessage form={this.state} errors={errors} onSubmit={this.onLogin} />;
      case 'verify':
        return <VerifyForm form={this.state} errors={errors} onSubmit={this.verifyCode} />;
      case 'reset':
        return <ChangePasswordForm form={this.state} errors={errors} onSubmit={this.changePassword} />;
      default:
        return <ForgotForm form={this.state} errors={errors} onSubmit={this.sendCode} />
    }
  }
}

