import React from 'react'
import {
  Container,
  View,
  Text,
  Button,
  Form,
  Item,
  Label,
  Input,
  Icon,
  Left,
  Body,
  Title,
  Right,
  Header
} from 'native-base';
import styles from './loginStyles'
import { Loader, Helpers, Config } from 'my-components'
import {Facebook} from "expo";

export default class Login extends React.Component {
  state = {
    username: '',
    password: '',
    sending: false,
    errors: {}
  };

  componentDidMount = () => {

  };

  back = () => {
    return this.props.navigation.goBack();
  };

  onFogot = () => {
    return this.props.navigation.navigate('ForgotPassword');
  };

  submit = () => {
    this.setState({
      sending: true
    });
    const {username, password}  = this.state;
    this.props.login(username, password).then(() => {

    }).catch((ex) => {
      console.log(ex.message, ex);
      switch (ex.response.status) {
        case 422:
          this.setState({errors: ex.response.data.errors});
          Helpers.showToast({type: 'error', message: "Invalid username or password"});
          break;
        case 430:
          Helpers.showToast({type: 'error', message: ex.response.data.message});
          break;
        case 500:
          Helpers.showToast({type: 'error', message: ex.response.data.message});
          break;
        default:
          console.log(ex);
          break;
      }
    }).finally(() => {
      this.setState({
        sending: false
      });

      const {user} =  this.props;
      if(!!user){
        if(!user.phone)
          return this.props.navigation.navigate('UpdatePhone');

        return this.props.navigation.navigate('App');
      }
    })
  };




  render() {
    const { errors } =  this.state;
    return (
      <Container  style={styles.container}>
        <Header iosBarStyle="dark-content" androidStatusBarColor={'#F1F3F5'}>
          <Left>
            <Button transparent onPress={this.back}>
              <Icon name='menu' android="md-arrow-back" ios="ios-arrow-back"/>
            </Button>
          </Left>
          <Body>
          <Title>Sign In</Title>
          </Body>
          <Right/>
        </Header>
        <Loader
          loading={this.state.sending} />
        <View style={styles.headerContainer}>
          <Text style={styles.headerText}>Sign In</Text>
          <Form>
            <Item floatingLabel input error={Helpers.hasError(errors, 'username')}>
              <Label>Phone No.</Label>
              <Input
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="phone-pad"
                returnKeyType="next"
                value={this.state.username}
                onChangeText={(username) => this.setState({username})}/>
              {Helpers.hasError(errors, 'username') && <Icon name='close-circle' />}
            </Item>
            {Helpers.renderError(errors, 'username')}
            <Item floatingLabel input error={Helpers.hasError(errors, 'password')}>
              <Label>Password</Label>
              <Input
                value={this.state.password}
                secureTextEntry={true}
                onChangeText={(password) => this.setState({password})}/>
              {Helpers.hasError(errors, 'password') && <Icon name='close-circle' />}
            </Item>
            {Helpers.renderError(errors, 'password')}
            <Item noBorder noIndent style={{marginTop: 20, marginLeft: 0, padingLeft: 0}} onPress={this.onFogot}>
              <Text note>Forgot password?</Text>
            </Item>
          </Form>
        </View>
        <View style={styles.footer}>
          <Button danger block style={styles.btnSignIn} onPress={this.submit}>
            <Text>Sign In</Text>
          </Button>
        </View>
      </Container>
    )
  }
}

