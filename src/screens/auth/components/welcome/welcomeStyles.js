import {Theme } from 'my-components'
const styles = {
  container: {
    backgroundColor: '#0610A1',
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    position: 'relative',
    alignItems: 'stretch'
  },
  headerText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  headerContainer: {
    flex: 3.5,
    position: 'relative',
    justifyContent: 'center',
    alignItems: 'center'
  },
  footer: {
    padding: 20,
    marginTop: 100,
    flexDirection: 'column',
    backgroundColor: 'transparent',
    zIndex: -1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: '100%'
  },
  btnSignUp: {
    marginVertical: 10
  },
  btnSignIn: {
    marginVertical: 10
  },
  canvas: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  fullImage: {
    height: '100%',
  },
  textContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    paddingTop: 200,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
  },
  subTitle: {
    color: '#fff',
    fontSize: 20
  },
  title: {
    color: '#fff',
    fontSize: 50,
    marginVertical: '3%',
    marginBottom: '10%'
  },
  button: {
    alignSelf:  'center',
    backgroundColor: Theme.brandPrimary,
    // marginTop: '10%',
    justifyContent: 'center',
    alignItems: 'center'
  }
};

export default styles;