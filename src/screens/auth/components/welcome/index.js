import React from 'react'
import { Image, ImageBackground } from 'react-native'
import {View, Text, Button, Spinner, Header, Container } from 'native-base';
import styles from './welcomeStyles'
import {Splash} from 'my-components'

const image  =  require('../../../../assets/images/main-bg.png');

export default class Welcome extends React.Component {
  state = {
    ready: false,
    authed: false
  };

  componentWillMount = () => {
    const {user, myProfile} = this.props;
    if(!user){
      this.setState({ ready: true})
    }

    myProfile().then( () => {
      this.setState({ ready: true})
    }).catch((ex) => {
      this.setState({ ready: true})
    }).finally(() => {
      if(!!user){
        if(!user.phone)
          return this.props.navigation.navigate('UpdatePhone');

        return this.props.navigation.navigate('App');
      }
    })
  };

  login = () => {
    return this.props.navigation.navigate("Login");
  };

  register = () => {
    return this.props.navigation.navigate("Register");
  };

  render() {
    if(!this.state.ready){
      return <Splash />
    }

    return (
      <Container style={styles.container} resizeMode='cover'>
        <View style={styles.textContainer}>
          <Text style={styles.subTitle}>Welcome to</Text>
          <Text style={styles.title}>Dharura</Text>
          <View style={styles.footer}>
            <Button primary block style={styles.btnSignIn} onPress={this.login}>
              <Text>Sign in</Text>
            </Button>
          </View>
        </View>
      </Container>
    )
  };
}

