import React from 'react'
import {
  Container,
  View,
  Text,
  Button,
  Form,
  Item,
  Label,
  Input,
  Icon,
  Left,
  Body,
  Title,
  Right,
  Header,
  Picker,
  Content
} from 'native-base';
import styles from './updatePhoneStyles'
import { Loader, Helpers, Config } from 'my-components'
import { Facebook } from "expo";

export default class UpdatePhone extends React.Component {
  state = {
    phone: '',
    sending: false,
    errors: {}
  };

  back = () => {
    return this.props.navigation.goBack();
  };

  submit = () => {
    this.setState({
      sending: true
    });
    const {phone}  = this.state;
    this.props.profile({phone}).then(() => {

    }).catch((ex) => {
      console.log(ex.message, ex);
      switch (ex.response.status) {
        case 422:
          this.setState({errors: ex.response.data.errors});
          break;
        case 430:
          Helpers.showToast({type: 'error', message: ex.response.data.message});
          break;
        case 500:
          Helpers.showToast({type: 'error', message: ex.response.data.message});
          break;
        default:
          console.log(ex);
          break;
      }
    }).finally(() => {
      this.setState({
        sending: false
      });

      const {user} =  this.props;
      if(!!user){
        if(!user.phone)
          return this.props.navigation.navigate('UpdatePhone');

        return this.props.navigation.navigate('App');
      }
    })
  };

  render() {
    const { errors } = this.state;
    return (
      <Container  style={styles.container}>
        <Header iosBarStyle="dark-content" androidStatusBarColor={'#F1F3F5'}>
          <Left>
            <Button transparent onPress={this.back}>
              <Icon name='menu' android="md-arrow-back" ios="ios-arrow-back"/>
            </Button>
          </Left>
          <Body>
          <Title>Update</Title>
          </Body>
          <Right/>
        </Header>
        <Loader
          loading={this.state.sending} />
        <Content padder>
          <Form>
            <Item floatingLabel input  error={Helpers.hasError(errors, 'phone')}>
              <Label>Phone Number</Label>
              <Input
                value={this.state.email}
                keyboardType="phone-pad"
                onChangeText={(phone) => this.setState({phone})}/>
              {Helpers.hasError(errors, 'phone') && <Icon name='close-circle' />}
            </Item>
            {Helpers.renderError(errors, 'phone')}

          </Form>
          <View style={styles.footer}>
            <Button danger block style={styles.btnSignIn} onPress={this.submit}>
              <Text>Continue</Text>
            </Button>
          </View>
        </Content>

      </Container>
    )
  }
}

