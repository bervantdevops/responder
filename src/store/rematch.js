
import { init } from "@rematch/core";
import * as models from './models'
import createRematchPersist from '@rematch/persist'
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import storage from "redux-persist/lib/storage";

const persistConfig = {
  version: 1,
  key: 'root_x',
  storage: storage,
  stateReconciler: autoMergeLevel2 // see "Merge Process" section for details.
};

const persistPlugin = createRematchPersist(persistConfig);

export const store = init({
  models,
  plugins: [persistPlugin]
});
