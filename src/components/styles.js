import {StyleSheet, StatusBar, Alert, Platform, Dimensions} from 'react-native';
const platform = Platform.OS == "android"
    ? "Android"
    : "IOS";
const {height, width} = Dimensions.get('window');
import { Constants } from 'expo';
const tintColor = "#fb0206";
const COLOR_DEFAULT = '#333333'
const COLOR_PRIMARY = '#673ab7'
const COLOR_SUCCESS = '#1DB895'
const COLOR_DANGER = '#FE4282'
const COLOR_WARNING = '#FAB534'
const COLOR_MUTED = '#9d9d9d'
const COLOR_LIGHT = '#F9F9F9'

export const constants = {
    composeBorderColor: "#F5F5F5",
    mainBackgroundColor: "#ffffff",
    mainTintColor: tintColor,
    mainDimColor: "#9d9d9d",
    mainTextColor: "#000000",
    mainTextFontSize: 16,
    mainHeaderFontSize: 16,
    navBarColor: "white",
    navBarTextColor: tintColor,
    navBarColorAndroid: tintColor,
    navBarTextColorAndroid: "#ffffff",
    paddingLeft: 14,
    plainCellBorderColor: "#efefef",
    sectionedCellHorizontalPadding: 14,
    selectedBackgroundColor: "#efefef",
    maxSettingsCellHeight: 45,
    primaryColor: COLOR_PRIMARY,
    successColor: COLOR_SUCCESS,
    warningColor: COLOR_WARNING,
    dangerColor: COLOR_DANGER,
    defaultColor: COLOR_DEFAULT,
    mutedColor: COLOR_MUTED,
    mutedLightColor: COLOR_LIGHT,
    deviceHeight: height,
    deviceWidth: width
}

export const styles = {
    container: {
        backgroundColor: constants.mainBackgroundColor,
        height: "100%"
    },
    pStatusBar: {
        paddingTop: Platform.OS === 'ios'
            ? 0
            : Constants.statusBarHeight
    },
    fullScreen: {
        backgroundColor: constants.mainBackgroundColor,
        height: height,
        width: width,
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    flexContainer: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: constants.mainBackgroundColor
    },

    centeredContainer: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    flexedItem: {
        flexGrow: 1
    },

    uiText: {
        color: constants.mainTextColor,
        fontSize: constants.mainTextFontSize
    },

    view: {
        backgroundColor: constants.mainBackgroundColor
    },

    tableSection: {
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: constants.mainBackgroundColor
    },

    sectionHeader: {
        fontSize: constants.mainTextFontSize - 4,
        paddingLeft: constants.paddingLeft,
        paddingBottom: 10,
        paddingTop: 10,
        color: constants.mainDimColor,
        fontWeight: Platform.OS == "android"
            ? "bold"
            : "normal"
    },

    sectionHeaderAndroid: {
        fontSize: constants.mainTextFontSize - 2,
        color: constants.mainTintColor
    },

    sectionedTableCell: {
        borderBottomColor: constants.plainCellBorderColor,
        borderBottomWidth: 1,
        paddingLeft: constants.paddingLeft,
        paddingRight: constants.paddingLeft,
        paddingTop: 13,
        paddingBottom: 12,
        backgroundColor: constants.mainBackgroundColor,
        flex: 1
    },

    textInputCell: {
        maxHeight: 50
    },

    sectionedTableCellTextInput: {
        fontSize: constants.mainTextFontSize,
        padding: 0,
        color: constants.mainTextColor
    },

    sectionedTableCellFirst: {
        borderTopColor: constants.plainCellBorderColor,
        borderTopWidth: 1
    },

    sectionedTableCellLast: {},

    sectionedTableCellFirstAndroid: {
        borderTopWidth: 0
    },

    sectionedTableCellLastAndroid: {
        borderBottomWidth: 0,
        borderTopWidth: 0
    },

    sectionedAccessoryTableCell: {
        paddingTop: 0,
        paddingBottom: 0,
        minHeight: 47
    },

    sectionedAccessoryTableCellLabel: {
        fontSize: constants.mainTextFontSize,
        color: constants.mainTextColor,
        minWidth: "80%"
    },

    buttonCell: {
        paddingTop: 0,
        paddingBottom: 0,
        flex: 1,
        justifyContent: 'center'
    },

    buttonCellButton: {
        textAlign: "center",
        textAlignVertical: "center",
        color: Platform.OS == "android"
            ? constants.mainTextColor
            : constants.mainTintColor,
        fontSize: constants.mainTextFontSize
    },

    buttonCellButtonLeft: {
        textAlign: "left"
    },

    noteText: {
        flexGrow: 1,
        marginTop: 0,
        paddingTop: 10,
        color: constants.mainTextColor,
        paddingLeft: constants.paddingLeft,
        paddingRight: constants.paddingLeft,
        paddingBottom: 10
    },

    noteTextIOS: {
        paddingLeft: constants.paddingLeft - 5,
        paddingRight: constants.paddingLeft - 5
    },

    syncBar: {
        position: "absolute",
        bottom: 0,
        width: "100%",
        backgroundColor: constants.mainTextColor,
        padding: 5
    },

    syncBarText: {
        textAlign: "center",
        color: constants.mainBackgroundColor
    },

    actionSheetWrapper: {},

    actionSheetOverlay: {
        // This is the dimmed background backgroundColor: constants.mainDimColor
    },

    actionSheetBody: {
        // This will also set button border bottoms, since margin is used instead of
        // borders
        backgroundColor: constants.plainCellBorderColor
    },

    actionSheetTitleWrapper: {
        backgroundColor: constants.mainBackgroundColor,
        marginBottom: 1
    },

    actionSheetTitleText: {
        color: constants.mainTextColor,
        opacity: 0.5
    },

    actionSheetButtonWrapper: {
        backgroundColor: constants.mainBackgroundColor,
        marginTop: 0
    },

    actionSheetButtonTitle: {
        color: constants.mainTextColor
    },

    actionSheetCancelButtonWrapper: {
        marginTop: 0
    },

    actionSheetCancelButtonTitle: {
        color: constants.mainTintColor,
        fontWeight: "normal"
    },

    bold: {
        fontWeight: "bold"
    },
    row: {
        //flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    box: {
        //flex: 1,
        minHeight: 20
    },
    input: {
        marginTop: 5,
        marginBottom: 5
    },
    card: {
        borderWidth: 0,
        backgroundColor: '#fff',
        borderColor: 'rgba(0,0,0,0.1)',
        margin: 5,
        minHeight: 50,
        padding: 5,
        elevation: 4,
        shadowColor: '#000',
        shadowOpacity: 1,
        shadowRadius: 5,
        shadowOffset: {
            width: 0,
            height: 3
        }
    },

    h1: {
        fontSize: 30
    },
    h2: {
        fontSize: 24
    },
    h3: {
        fontSize: 18
    },
    h4: {
        fontSize: 12
    },
    h5: {
        fontSize: 10
    },
    textPrimary: {
        color: COLOR_PRIMARY
    },
    textSuccess: {
        color: COLOR_SUCCESS
    },
    textWarning: {
        color: COLOR_WARNING
    },
    textDanger: {
        color: COLOR_DANGER
    },
    textDefault: {
        color: COLOR_DEFAULT
    },
    textMuted: {
        color: '#9EA1A0'
    },
    button: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center'
    },
    buttonSuccess: {
        backgroundColor: COLOR_SUCCESS,
        color: '#FFF',
        fontSize: 14
    },
    buttonPrimary: {
        backgroundColor: COLOR_PRIMARY,
        color: '#FFF',
        fontSize: 14
    },
    buttonDefault: {
        backgroundColor: COLOR_DEFAULT,
        color: '#FFF',
        fontSize: 14
    },
    buttonTransparentDefault: {
        backgroundColor: 'transparent',
        color: COLOR_DEFAULT,
        fontSize: 14
    },
    buttonTransparentSuccess: {
        backgroundColor: 'transparent',
        color: COLOR_SUCCESS,
        fontSize: 14
    },
    buttonRound: {
        borderRadius: 10
    },
    p: {
        paddingTop: 2,
        paddingBottom: 2,
        paddingLeft: 1,
        paddingRight: 1
    },
    textRight: {
        textAlign: 'right'
    },
    textLeft: {
        textAlign: 'left'
    },
    textCenter: {
        textAlign: 'center'
    },
    textWhite: {
        color: '#FFFFFF'
    },
    bgPrimary: {
        backgroundColor: COLOR_PRIMARY
    },
    bgSuccess: {
        backgroundColor: COLOR_SUCCESS
    },
    bgDanger: {
        backgroundColor: COLOR_DANGER
    },
    bgWarning: {
        backgroundColor: COLOR_WARNING
    },
    bgDefault: {
        backgroundColor: COLOR_DEFAULT
    },
    bgWhite: {
        backgroundColor: '#fff'
    },
    bgMuted: {
        backgroundColor: COLOR_MUTED
    },
    bgLight: {
        backgroundColor: COLOR_LIGHT
    },
    circle: {
        borderRadius: 20
    },
    tab: {
        flex: 1,
        padding: 5
    },
    tabView: {
        flex: 1,
        backgroundColor: '#FFF'
    },
    textBold: {
        fontWeight: 'bold'
    },
    bottomModal: {
        justifyContent: 'flex-end',
        margin: 0
    },
    modalContent: {
        backgroundColor: 'white',
        padding: 22,
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)'
    }
}

export function shadeBlend(p, c0, c1) {
    var n = p < 0
            ? p * -1
            : p,
        u = Math.round,
        w = parseInt;
    if (c0.length > 7) {
        var f = c0.split(","),
            t = (c1
                ? c1
                : p < 0
                    ? "rgb(0,0,0)"
                    : "rgb(255,255,255)").split(","),
            R = w(f[0].slice(4)),
            G = w(f[1]),
            B = w(f[2]);
        return "rgb(" + (u((w(t[0].slice(4)) - R) * n) + R) + "," + (u((w(t[1]) - G) * n) + G) + "," + (u((w(t[2]) - B) * n) + B) + ")"
    } else {
        var f = w(c0.slice(1), 16),
            t = w((c1
                ? c1
                : p < 0
                    ? "#000000"
                    : "#FFFFFF").slice(1), 16),
            R1 = f >> 16,
            G1 = f >> 8 & 0x00FF,
            B1 = f & 0x0000FF;
        return "#" + (0x1000000 + (u(((t >> 16) - R1) * n) + R1) * 0x10000 + (u(((t >> 8 & 0x00FF) - G1) * n) + G1) * 0x100 + (u(((t & 0x0000FF) - B1) * n) + B1))
            .toString(16)
            .slice(1)
    }
}

export function darken(color, value = -0.15) {
    return this.shadeBlend(value, color);
}

export function lighten(color, value = 0.25) {
    return this.shadeBlend(value, color);
}

export function hexToRGBA(hex, alpha) {
    var c;
    if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
        c = hex
            .substring(1)
            .split('');
        if (c.length == 3) {
            c = [
                c[0],
                c[0],
                c[1],
                c[1],
                c[2],
                c[2]
            ];
        }
        c = '0x' + c.join('');
        return 'rgba(' + [
            (c >> 16) & 255,
            (c >> 8) & 255,
            c & 255
        ].join(',') + ',' + alpha + ')';
    } else {
        throw new Error('Bad Hex');
    }
}
