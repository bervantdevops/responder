import React, {Component} from 'react';
import {
    View, Image, Text
} from 'react-native';
import {styles } from '../components/styles'
import {EvilIcons, MaterialCommunityIcons as MCIcons} from '@expo/vector-icons'
import moment from 'moment'

const staticAvatar = 'https://randomuser.me/api/portraits/men/83.jpg';

export default class UserCardComponent extends Component {
    componentDidMount(){

    }

    render() {
        let user  = this.props.user;

        return (
            <View style={[ styles.row, { padding: 5, height: 100}]}>
                <View style={[ styles.box, styles.circle, {width: 70, height: 70}]}>
                <Image
                        style={[styles.circle, {width: 70, height: 70}]}
                        source={{ uri: staticAvatar }}
                    />
                </View>
                <View style={[ styles.box, { flex: 2, paddingLeft: 3 }]}>
                    <Text style={[styles.h3, styles.textPrimary]}>{user && user.name}</Text>
                    <Text style={[ ]}><EvilIcons name="location" size={15}/> {user.phone}</Text>
                    <Text style={[ ]}><EvilIcons name="calendar" size={15}/> {moment().format('DD MMM, h:m A')}</Text>
                </View>
                <View style={[ styles.box, { height: 45, padding: 5}]}>
                    <View style={[{flex: 1, flexDirection: 'row'}]}>
                        <View style={[ styles.bgSuccess, styles.circle, { padding: 8, marginRight: 3} ]}>
                            <MCIcons name="message-text-outline" color={'#FFF'} size={20}/>
                        </View>
                        <View style={[ styles.bgSuccess, styles.circle, { padding: 5, marginLeft: 3} ]}>
                            <MCIcons  name="phone"  color={'#FFF'} size={25}/>
                        </View>

                    </View>
                </View>
            </View>
        );
    }
}



